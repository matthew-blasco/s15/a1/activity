
// 2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
console.log (`Hello World`);

// 3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
let firstNum = +prompt("Enter first number");
let secondNum = +prompt("Enter second number");
let resultNum = (firstNum + secondNum);


	// 3. a. If the total of two numbers is less than 10, add the numbers
	if(resultNum < 10){
		alert(`The sum of the two numbers is: ` + resultNum);
		//4. Use an alert for the total of 10 or greater and console warning for total of 9 or less
		console.warn(resultNum);
	// 3. b. If the total of the two numbers is 10-20, subtract the numbers
	} else if(resultNum <= 20){
		alert(`The difference of your number is: ` + (firstNum - secondNum));
	// 3. c. If the total of the two numbers is 21-29, multiply the numbers
	} else if(resultNum <= 29){
		alert(`The product of the two numbers is: ` + (firstNum * secondNum));
	// 3. d. If the total of the two numbers is greater than or equal to 30, divide the numbers
	} else if(resultNum >= 30){
		alert(`The quotient of the two numbers is: ` + (firstNum / secondNum));
	}
	else {
		console.log(`What number is that?`)
	};


let name = prompt(`What is your name?`);
let age = parseInt(prompt(`How about your age?`));


	// Prompt the user for their name and age and print out different alert messages based on the user input:
	// a. if the name OR age is blank//null, print "are you a time traveler?"
if(name === "" || age === ""){
	alert(`Are you a time traveler?`);
} 
	// b. if the name AND age is not blank, print the message with the user's name and age
else{
	alert(`Welcome `+ name + ', you are ' + age + ' years old. Right?')
};

// 6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
	// a. 18 or greater, print an alert saying "You are legal age".
function isLegalAge(age){
	if (age >= 18) {
		alert(`You are of legal age.`)
	} 
	// b. 17 or less, print an alert saying "You are not allowed here."
	else {
		alert(`You are not allowed here.`)
	};
};

isLegalAge(age);

// 7. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
	// a. 18 - print the message "You are allowed to party"
	// b. 21 - print the message "You are now part of the adult society"
	// c. 65 - print the message "We thank you for your contribution to society"
	// d. Any other value - print the message "Are you sure you're not an alien?"
		
		switch(age){
		case age = 18: alert(`You are allowed to party`);
				break; 
		case age = 21: alert(`You are now part of the adult society`);		
				break; 
		case age = 65: alert(`We thank you for your contribution to society`);
				break; 
		default: alert (`Are you sure you're not an alien?`);
		}